<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>

<html>

<head>
	<title>Student Registration Form</title>
</head>

<body>
	<form:form action="processForm" method="get" modelAttribute="student">
		First Name: <form:input path="firstName"/>
		
		<br /><br />
		
		last Name: <form:input path="lastName"/>
		
		<br /><br />
		
		<form:select path="country">
			<form:options items="${student.countryOptions}" />
		</form:select>
		
		<br /><br />
		
		What's your favorite language?
		<br /><br />
		<form:radiobuttons path="favoriteLanguage" items="${student.favoriteLanguageOptions }" />
		
		<br /><br />
		
		Linux<form:checkbox path="operatingSystems" value="Linux"/>
		Windows<form:checkbox path="operatingSystems" value="Windows"/>
		Android<form:checkbox path="operatingSystems" value="Android"/>
		Mac<form:checkbox path="operatingSystems" value="Mac"/>
		
		<br /><br />
		<input type="submit" value="Register">
	</form:form>
</body>