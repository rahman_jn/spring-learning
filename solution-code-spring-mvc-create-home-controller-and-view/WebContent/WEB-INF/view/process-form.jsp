<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DocType html>
<html>
	<body>
		<h2>
			The student is confirmed: ${student.firstName} ${student.lastName}
		</h2>
		<h3>
			Country : ${student.country }
		</h3>
		<h3>
			Favorite language: ${student.favoriteLanguage }
		</h3>
		
		<ul>
	<c:forEach var="temp" items="${student.operatingSystems}">

		<li> ${temp} </li>

	</c:forEach>
</ul>

	</body>
</html>