package com.luv2code.springdemo.mvc;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("hello")
public class HelloController {

	//Init method to show the form
	@RequestMapping("/helloform")
	public String showForm() {
		return "helloform";
	}
	
	//Process the form
	@RequestMapping("/processForm")
	public String responseForm() {
		return  "responseform";
	}
	
	//Store data in model
	@RequestMapping("/processFormvtwo")
	public String ShoutDude(HttpServletRequest request, Model model) {
		
		String theName = request.getParameter("stdname");
		theName = theName.toUpperCase();
		String result = "Yo! " + theName;
		model.addAttribute("message", result);
		return "responseformvtwo";
	}
	
	//Store data in model
	@RequestMapping("/processFormvthree")
	public String ShoutDudeVthree(@RequestParam("stdname") String theName, Model model) {
		
		String result = "Hello from v3! " + theName;
		model.addAttribute("message", result);
		return "responseformvtwo";
	}
}
