package com.luv2code.springdemo.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController {

	//Show student registration form
	@RequestMapping("/showForm")
	public String showForm(Model theModel) {
		//Create the student object
		Student theStudent = new Student();
		
		//bind data to model
		theModel.addAttribute("student", theStudent);	
		return "student-form";
	}
	
	//Process input values from student registration page
	@RequestMapping("/processForm")
	public String processForm(@ModelAttribute("student") Student theStudent) {
		System.out.println("Student Name is: " + theStudent.getFirstName() + " " + theStudent.getLastName());
		return "process-form";
	}
}
