package com.luv2code.springdemo.mvc;

import java.util.LinkedHashMap;

public class Student {

	//Student attributes
	private String firstName;

	private String lastName;
	private String country;
	private String favoriteLanguage;

	private LinkedHashMap<String , String > countryOptions;
	private LinkedHashMap<String, String> favoriteLanguageOptions;
	
	private String[] operatingSystems;
	

	public  Student() {
		
		//Country list for displaying in dropdown list
		countryOptions = new LinkedHashMap<>();
		countryOptions.put("IR", "Iran");
		countryOptions.put("US", "USA");
		countryOptions.put("NETH", "Netherland");
		
		//Favorite languages values, initialized by 4 programming language for displaying in radio button
		favoriteLanguageOptions = new LinkedHashMap<>();
		favoriteLanguageOptions.put("PHP", "php");
		favoriteLanguageOptions.put("JAVA", "java");
		favoriteLanguageOptions.put("C#", "c#");
		favoriteLanguageOptions.put("RUBY", "ruby");
		
	}
	
	public String[] getOperatingSystems() {
		return operatingSystems;
	}

	public void setOperatingSystems(String[] operatingSystems) {
		this.operatingSystems = operatingSystems;
	}

	public LinkedHashMap<String, String> getCountryOptions() {
		return countryOptions;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public LinkedHashMap<String, String> getfavoriteLanguageOptions() {
		return favoriteLanguageOptions;
	}
	
	public String getFavoriteLanguage() {
		return favoriteLanguage;
	}

	public void setFavoriteLanguage(String favoriteLanguage) {
		this.favoriteLanguage = favoriteLanguage;
	}
}
