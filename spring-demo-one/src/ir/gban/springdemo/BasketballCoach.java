package ir.gban.springdemo;

public class BasketballCoach implements Coach {

	private FortuneService fortuneService;
	private String email;
	private String team;
	@Override
	public String getDailyWork() {
		// TODO Auto-generated method stub
		return "Daily work of Basketball Coach";
	}

	@Override
	public String getDailyFortune() {
		// TODO Auto-generated method stub
		return fortuneService.getFortune();
	}

	public void setFortuneService(FortuneService fortuneService) {
		System.out.println("BasketBallCoach : inside fortuneService setter");
		this.fortuneService = fortuneService;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		System.out.println("BasketBallCoach : inside email setter");
		this.email = email;
	}

	public String getTeam() {
		System.out.println("BasketBallCoach : inside team setter");
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

}
