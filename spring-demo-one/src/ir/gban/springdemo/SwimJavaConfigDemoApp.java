package ir.gban.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SwimJavaConfigDemoApp {

	public static void main(String[] args) {
		//Load Java configuration
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);
		
		//Get the bean from config file
		SwimCoach theCoach = context.getBean("mySwimCoach", SwimCoach.class);
		
		//Print output
		System.out.println(theCoach.getDailyWork());
		
		System.out.println(theCoach.getDailyFortune());
		
		System.out.println("Player team is: " + theCoach.getTeam());
		
		System.out.println("Player email is: " + theCoach.getEmail());
		
		//Close the context
		context.close();
		

	}

}
