package ir.gban.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class HElloSpringApp {

	public static void main(String[] args) {
		// Load the spring configurationfile
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		//Retrive ban from spring container
		Coach theCoach = context.getBean("myCoach", Coach.class);
		//Call methodson the bean
		System.out.println(theCoach.getDailyWork());
		
		System.out.println("You Are Here: " + theCoach.getDailyFortune());
		//Close the context
		context.close();
	}

}
