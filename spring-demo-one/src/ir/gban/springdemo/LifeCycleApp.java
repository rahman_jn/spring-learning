package ir.gban.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LifeCycleApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Load container configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanLifeCycle-applicationContext.xml");
		
		//Create instance from bean
		Coach theCoach = context.getBean("myCoach", Coach.class);
		System.out.println(theCoach.getDailyWork());
		
		//close container
		context.close();
	}

}
