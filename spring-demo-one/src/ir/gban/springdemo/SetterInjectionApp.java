package ir.gban.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SetterInjectionApp {

	public static void main(String[] args) {
		
		//Load configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		//Get bean from configuration
		BasketballCoach theCoach = context.getBean("basketballCoach", BasketballCoach.class);
		
		//Print Output
		System.out.println(theCoach.getDailyFortune());
		
		//Print literal refrence
		System.out.println(theCoach.getEmail());
		System.out.println(theCoach.getTeam());
	}

}
