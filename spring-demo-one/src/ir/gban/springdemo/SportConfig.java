package ir.gban.springdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:sport.properties")
//@ComponentScan("ir.gban.springdemo")
public class SportConfig {
	
	@Bean
	public FortuneService mySadFortune() {
		return new SadFortuneService();
	}
	
	@Bean
	public Coach mySwimCoach(FortuneService sadFortuneService) {
		return new SwimCoach(sadFortuneService);
	}
}
