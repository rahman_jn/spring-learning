package ir.gban.springdemo;

public class FootballCoach implements Coach{
	
	private FortuneService fortuneService;
	
	//Initializer method for running befor bean start
	public void Init() {
		System.out.println("This is initializer mehod");
	}
	
	//Inject Fortune dependency to constructor
	public FootballCoach(FortuneService theFortuneservice) {
		fortuneService = theFortuneservice;
	}
		
	//Daily Time sheet for football coach
	public String  getDailyWork() {
		return ("Football Coach has 30 hours work");
	}

	@Override
	public String getDailyFortune() {
		
		return fortuneService.getFortune();
	}

	//Destroy method for run before context close
	public void DestroyIt() {
		System.out.println("Here is Destroy method");
	}
}
