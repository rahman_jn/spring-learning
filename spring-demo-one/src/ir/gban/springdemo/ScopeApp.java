package ir.gban.springdemo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ScopeApp {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//Load container configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beanScope-applicationContext.xml");
		
		//Create instance from bean
		Coach theCoach = context.getBean("myCoach", Coach.class);
		Coach alphaCoach = context.getBean("myCoach",Coach.class);
		
		boolean result = (theCoach == alphaCoach);
		
		System.out.println("Memory address for theCoach is: " + theCoach);
		System.out.println("Memory address for alphaCoach is: " + alphaCoach);
		
		//close container
		context.close();
	}

}
