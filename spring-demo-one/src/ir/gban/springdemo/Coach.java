/**
 * 
 */
package ir.gban.springdemo;

/**
 * @author rahman
 *
 */
public interface Coach {
	//Coach timesheet
	public String getDailyWork();
	public String getDailyFortune();
}
