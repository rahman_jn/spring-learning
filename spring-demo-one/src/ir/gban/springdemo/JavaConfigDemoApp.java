package ir.gban.springdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class JavaConfigDemoApp {

	public static void main(String[] args) {
		//Load Java configuration
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SportConfig.class);
		
		//Get the bean from config file
		Coach theCoach = context.getBean("tennisCoach", Coach.class);
		
		//Print output
		System.out.println(theCoach.getDailyWork());
		
		//Close the context
		context.close();
		

	}

}
